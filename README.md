# kernel

Forked version of kernel with ClearOS changes applied

```
git clone --branch c7 https://git.centos.org/rpms/kernel.git centos-kernel
git clone git@gitlab.com:clearos/upstream/centos/kernel.git
cd kernel
git checkout c7
git remote add upstream ../centos-kernel
git pull upstream c7:c7
git checkout clear7
git merge --no-commit c7
git commit
git push
```

# kernel instructions (deprecated)

Forked version of kernel with ClearOS changes applied

* git clone git+ssh://git@github.com/clearos/kernel.git
* cd kernel
* git checkout c7
* git remote add upstream git://git.centos.org/rpms/kernel.git
* git pull upstream c7
* git checkout clear7
* git merge --no-commit c7
* git commit

To fetch sources:

* ln -sf ../common/Makefile
* make srpm
